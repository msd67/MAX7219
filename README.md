MAX 7219
The MAX7219/MAX7221 are compact, serial input/output
common-cathode display drivers that interface
microprocessors (μPs) to 7-segment numeric LED displays
of up to 8 digits, bar-graph displays, or 64 individual
LEDs. Included on-chip are a BCD code-B
decoder, multiplex scan circuitry, segment and digit
drivers, and an 8x8 static RAM that stores each digit.
Only one external resistor is required to set the segment
current for all LEDs. The MAX7221 is compatible
with SPI™, QSPI™, and MICROWIRE™, and has slewrate-
limited segment drivers to reduce EMI.


this is just modeling (Can not be synthesized)