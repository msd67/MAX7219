LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;

ENTITY MAX IS
    PORT(
        -- SINGLE BIT INPUT
        CLOCK, DIN, LOAD    : IN    STD_LOGIC;
        DIG                 : OUT    STD_LOGIC_VECTOR(7 DOWNTO 0);
        SEG                 : OUT   STD_LOGIC_VECTOR(7 DOWNTO 0);
        DOUT                : OUT   STD_LOGIC
    );
END MAX;

ARCHITECTURE BEHAVIORAL OF MAX IS

    FUNCTION DECODER_B(DIGIT : STD_LOGIC_VECTOR;
                        DM   : STD_LOGIC)
                        RETURN STD_LOGIC_VECTOR IS
        VARIABLE BEDECODE : STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS=>'0');
    BEGIN
        IF (DM='0') THEN
            BEDECODE := DIGIT;
        ELSE
            CASE DIGIT(3 DOWNTO 0) IS
                WHEN "0000" => BEDECODE := DIGIT(7) & "1111110";
                WHEN "0001" => BEDECODE := DIGIT(7) & "0110000";
                WHEN "0010" => BEDECODE := DIGIT(7) & "1101101";
                WHEN "0011" => BEDECODE := DIGIT(7) & "1111001";
                WHEN "0100" => BEDECODE := DIGIT(7) & "0110011";
                WHEN "0101" => BEDECODE := DIGIT(7) & "1011011";
                WHEN "0110" => BEDECODE := DIGIT(7) & "1011111";
                WHEN "0111" => BEDECODE := DIGIT(7) & "1110000";
                WHEN "1000" => BEDECODE := DIGIT(7) & "1111111";
                WHEN "1001" => BEDECODE := DIGIT(7) & "1111011";
                WHEN "1010" => BEDECODE := DIGIT(7) & "0000001";
                WHEN "1011" => BEDECODE := DIGIT(7) & "1001111";
                WHEN "1100" => BEDECODE := DIGIT(7) & "0110111";
                WHEN "1101" => BEDECODE := DIGIT(7) & "0001110";
                WHEN "1110" => BEDECODE := DIGIT(7) & "1100111";
                WHEN "1111" => BEDECODE := DIGIT(7) & "0000000";
				WHEN OTHERS =>
            END CASE;
        END IF;
        RETURN BEDECODE;
    END DECODER_B;

    SIGNAL  SHIFT_IN    : STD_LOGIC_VECTOR(15 DOWNTO 0);
    SIGNAL  DIGIT0      : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  DIGIT1      : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  DIGIT2      : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  DIGIT3      : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  DIGIT4      : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  DIGIT5      : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  DIGIT6      : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  DIGIT7      : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  DECODEMODE  : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  INTENSITY   : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  SCANLIMIT   : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  SHUTDOWN    : STD_LOGIC_VECTOR(7 DOWNTO 0);
    SIGNAL  DISPLAYTEST : STD_LOGIC_VECTOR(7 DOWNTO 0);

    TYPE    DIG_OUT IS (DIG7, DIG1, DIG2, DIG3, DIG4, DIG5, DIG6, DIG0);
    SIGNAL  TO_DIG      : DIG_OUT;
	
	TYPE	DRIVE	IS (INITIAL, RUN);
	SIGNAL	DRV			: DRIVE;

    SIGNAL DIG_CNT      : INTEGER RANGE 0 TO 12500;
	SIGNAL INTNST_CNT	: INTEGER RANGE 0 TO 33;
	SIGNAL PULSE_WIDTH	: STD_LOGIC;

BEGIN

    PROCESS(CLOCK, LOAD)
    BEGIN
		
		DIG		<=	(OTHERS=>'0');
        IF (DISPLAYTEST(0)='1') THEN
            DIG     <=  (OTHERS=>'0');
            SEG     <=  (OTHERS=>'1');
        ELSIF (SHUTDOWN(0)='0') THEN
            DIG     <=  (OTHERS=>'1');
            SEG     <=  (OTHERS=>'0');
		ELSE
			CASE SCANLIMIT(2 DOWNTO 0) IS
				WHEN "000" =>	DIG <= "11111110";
				WHEN "001" =>	DIG <= "11111100";
				WHEN "010" =>	DIG <= "11111000";
				WHEN "011" =>	DIG <= "11110000";
				WHEN "100" =>	DIG <= "11100000";
				WHEN "101" =>	DIG <= "11000000";
				WHEN "110" =>	DIG <= "10000000";
				WHEN OTHERS =>	DIG <= "00000000";
			END CASE;
        END IF;

        IF RISING_EDGE(CLOCK) THEN
		
            SHIFT_IN    <=  SHIFT_IN(14 DOWNTO 0) & DIN;
			
			INTNST_CNT	<=	INTNST_CNT + 1;
			IF (INTNST_CNT>=32) THEN
				INTNST_CNT	<=	0;
			END IF;
			CASE INTENSITY(3 DOWNTO 0) IS
				WHEN "0000" =>
					IF (INTNST_CNT=1) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "0001" =>
					IF (INTNST_CNT=3) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "0010" =>
					IF (INTNST_CNT=5) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "0011" =>
					IF (INTNST_CNT=7) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "0100" =>
					IF (INTNST_CNT=9) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "0101" =>
					IF (INTNST_CNT=11) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "0110" =>
					IF (INTNST_CNT=13) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "0111" =>
					IF (INTNST_CNT=15) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "1000" =>
					IF (INTNST_CNT=17) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "1001" =>
					IF (INTNST_CNT=19) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "1010" =>
					IF (INTNST_CNT=21) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "1011" =>
					IF (INTNST_CNT=23) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "1100" =>
					IF (INTNST_CNT=25) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "1101" =>
					IF (INTNST_CNT=27) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "1110" =>
					IF (INTNST_CNT=29) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN "1111" =>
					IF (INTNST_CNT=31) THEN
						PULSE_WIDTH	<=	'0';
					END IF;
				WHEN OTHERS =>
			END CASE;

			-- SEND TRUE DATA TO SEGMENTS
            IF (SHUTDOWN(0)/='0') THEN
                DIG_CNT     <=  DIG_CNT + 1;
            END IF;
			-- 12500 CLOCK TO 800HZ
            IF (DIG_CNT=16) THEN
                DIG_CNT       <=  0;
                CASE TO_DIG IS
                    WHEN DIG0 =>
                        IF (SCANLIMIT="00000000") THEN
                            TO_DIG  <=  DIG0;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT0, DECODEMODE(0));
                            END IF;
                        ELSE
                            TO_DIG  <=  DIG1;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT1, DECODEMODE(1));
                            END IF;
                        END IF;
                    WHEN DIG1 =>
                        IF (SCANLIMIT="00000001") THEN
                            TO_DIG  <=  DIG0;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT0, DECODEMODE(0));
                            END IF;
                        ELSE
                            TO_DIG  <=  DIG2;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT2, DECODEMODE(2));
                            END IF;
                        END IF;
                    WHEN DIG2 =>
                        IF (SCANLIMIT="00000010") THEN
                            TO_DIG  <=  DIG0;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT0, DECODEMODE(0));
                            END IF;
                        ELSE
                            TO_DIG  <=  DIG3;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT3, DECODEMODE(3));
                            END IF;
                        END IF;
                    WHEN DIG3 =>
                        IF (SCANLIMIT="00000011") THEN
                            TO_DIG  <=  DIG0;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT0, DECODEMODE(0));
                            END IF;
                        ELSE
                            TO_DIG  <=  DIG4;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT4, DECODEMODE(4));
                            END IF;
                        END IF;
                    WHEN DIG4 =>
                        IF (SCANLIMIT="00000100") THEN
                            TO_DIG  <=  DIG0;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT0, DECODEMODE(0));
                            END IF;
                        ELSE
                            TO_DIG  <=  DIG5;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT5, DECODEMODE(5));
                            END IF;
                        END IF;
                    WHEN DIG5 =>
                        IF (SCANLIMIT="00000101") THEN
                            TO_DIG  <=  DIG0;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT0, DECODEMODE(0));
                            END IF;
                        ELSE
                            TO_DIG  <=  DIG6;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT6, DECODEMODE(6));
                            END IF;
                        END IF;
                    WHEN DIG6 =>
                        IF (SCANLIMIT="00000110") THEN
                            TO_DIG  <=  DIG0;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT0, DECODEMODE(0));
                            END IF;
                        ELSE
                            TO_DIG  <=  DIG7;
                            IF (DISPLAYTEST(0)/='1') THEN
                                SEG     <=  DECODER_B(DIGIT7, DECODEMODE(7));
                            END IF;
                        END IF;
                    WHEN DIG7 =>
                        TO_DIG  <=  DIG0;
                        IF (DISPLAYTEST(0)/='1') THEN
                            SEG     <=  DECODER_B(DIGIT0, DECODEMODE(0));
                        END IF;
                    WHEN OTHERS =>
                END CASE;
            END IF;

        END IF;

        IF FALLING_EDGE(CLOCK) THEN
            DOUT        <=  SHIFT_IN(15);
        END IF;

        IF RISING_EDGE(LOAD) THEN
            CASE SHIFT_IN(11 DOWNTO 8) IS
                WHEN "0001" =>
                    DIGIT0          <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "0010" =>
                    DIGIT1          <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "0011" =>
                    DIGIT2          <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "0100" =>
                    DIGIT3          <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "0101" =>
                    DIGIT4          <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "0110" =>
                    DIGIT5          <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "0111" =>
                    DIGIT6          <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "1000" =>
                    DIGIT7          <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "1001" =>
                    DECODEMODE      <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "1010" =>
                    INTENSITY       <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "1011" =>
                    SCANLIMIT       <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "1100" =>
                    SHUTDOWN        <=  SHIFT_IN(7 DOWNTO 0);
                WHEN "1111" =>
                    DISPLAYTEST     <=  SHIFT_IN(7 DOWNTO 0);
                WHEN OTHERS =>
            END CASE;
        END IF;

		-- INITIAL POWER-UP
		IF (DRV=INITIAL) THEN
		
			DRV				<=	RUN;
			DIGIT0          <=  (OTHERS=>'0');
			DIGIT1          <=  (OTHERS=>'0');
			DIGIT2          <=  (OTHERS=>'0');
			DIGIT3          <=  (OTHERS=>'0');
			DIGIT4          <=  (OTHERS=>'0');
			DIGIT5          <=  (OTHERS=>'0');
			DIGIT6          <=  (OTHERS=>'0');
			DIGIT7          <=  (OTHERS=>'0');
			DECODEMODE      <=  (OTHERS=>'0');
			INTENSITY       <=  (OTHERS=>'0');
			SCANLIMIT       <=  (OTHERS=>'0');
			SHUTDOWN        <=  (OTHERS=>'0');
			DISPLAYTEST     <=  (OTHERS=>'0');
			PULSE_WIDTH		<=	'1';
		
		END IF;
		
    END PROCESS;

END BEHAVIORAL;